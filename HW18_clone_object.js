"use strict";

const student = {

	firstName: "Ivan",
	lastName: "Ivanov",

	movies: {
		drama: ['Titanic', 'Beautiful Mind'],
		blockbuster: ['Matrix', 'Terminator'],

		cartoons: {
			"Chip & Dale": "cool",
			"Duck Tales": "cool",
		},
	},

	isLikeChipNDale() {
		return this.movies.cartoons["Chip & Dale"];
	},
};


function deepClone(obj) {
	const clone = {};
	for (const key in obj) {
		if (typeof obj[key] === 'object') {
			clone[key] = deepClone(obj[key]);
		} else {
			clone[key] = obj[key];
		}
	}
	return clone;
}


const newObj = deepClone(student);

console.log("object", student);
console.log("clone", newObj);

newObj.movies.cartoons["Chip & Dale"] = "sucks";

console.log("------------------------------------");

console.log("Object, do you like 'Chip & Dale?'", student.isLikeChipNDale());
console.log("Clone, do you like 'Chip & Dale?'", newObj.isLikeChipNDale());

